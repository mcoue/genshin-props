const GenshinCard = (props) => (
    <div className="card">
        <h2>{props.name}</h2>
        <span>{props.element}</span>
    </div>
)

export default GenshinCard;
