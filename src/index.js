import React from 'react';
import ReactDOM from 'react-dom/client';
import GenshinList from './GenshinList';
import './index.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <GenshinList />
  </React.StrictMode>
);
