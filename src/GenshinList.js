import {useState} from "react";
import GenshinCard from "./GenshinCard";

const GenshinList = () => {

    const [characters, ] = useState([
        {
            "name" : "Shogun Raiden",
            "element" : "Electro"
        },
        {
            "name" : "Ayaka",
            "element" : "Cryo"
        },
        {
            "name" : "Bennett",
            "element" : "Pyro"
        },
        {
            "name" : "Nahida",
            "element" : "Dendro"
        },
        {
            "name" : "Zhongli",
            "element" : "Geo"
        },
        {
            "name" : "Kokomi",
            "element" : "Hydro"
        },
        {
            "name" : "Venti",
            "element" : "Anemo"
        }
    ])

    return (
        <div className="container">
            <h1 className="title">My Genshin list</h1>
            <ul className="grid">
                {characters.map(character => (
                    <li><GenshinCard name={character.name} element={character.element} /></li>
                ))}
            </ul>
        </div>
    )
}

export default GenshinList;
